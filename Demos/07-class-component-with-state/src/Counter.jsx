import React, {Component} from 'react'

class Counter extends Component {
    /*
    //Not required anymore
    constructor() {
        super()
        this.state = {count: 0}
    } */
    state = {count: 0}
    addCount = () => this.setState({count: this.state.count + 1})
    componentDidMount() {
        document.title = `You clicked ${this.state.count} times`
    }
    componentDidUpdate() {
        document.title = `You clicked ${this.state.count} times`
    }
    render() {
        return (
            <div>
                <h3>You clicked {this.state.count} times</h3>
                <button onClick={this.addCount}>Click me!</button>
            </div>
        )
    }
}

export default Counter