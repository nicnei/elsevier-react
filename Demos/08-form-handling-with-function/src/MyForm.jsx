import React, {useState} from 'react'

const MyForm = () => {
    const [email, setEmail] = useState('')
    const [city, setCity] = useState('philadelphia')

    const handleEmailChange = (event) => {
        setEmail([event.target.value])
    }
    const handleCityChange = (event) => {
        setCity([event.target.value])
    }
    const handleSubmit = (event) => {
        alert(`${email} ${city}`)
        event.preventDefault() //stops browser clearing form
    }

    return (
        <form onSubmit={handleSubmit}>
                <div> 
                <label>Email: </label>
                <input type='email' value={email} onChange={handleEmailChange} required></input>
                </div>
                <div>
                <label>City: </label>
                <select value={city} onChange = {handleCityChange}>
                        <option value='philadelphia'>Philadelphia</option>
                        <option value='new york'>New York</option>
                        <option value='london'>London</option>
                </select>
                </div>
                <button type='submit'>Submit</button>
            </form>
    )
}

export default MyForm