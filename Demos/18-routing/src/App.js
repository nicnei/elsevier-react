import React from 'react';
import './App.css';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom';
import Home from './Home';
import Users from './Users';
import Contact from './Contact';

function App() {
  const routing = (
    <Router>
      <div className="container">
        <ul>
          <li> <Link to="/">Home</Link> </li>
          <li> <Link to="/users">Users</Link> </li>
          <li> <Link to="/contact">Contact</Link> </li>
        </ul>
        <Route path="/" component={Home} exact />
        <Route path="/users" component={Users} />
        <Route path="/contact" component={Contact} />
      </div>
    </Router>
  )

  return (
    <div className="App">
      {routing}
    </div>
  );
}

export default App;
