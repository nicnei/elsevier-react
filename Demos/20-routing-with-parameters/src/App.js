import React from 'react';
import './App.css';
import { Route, Link, BrowserRouter as Router, Switch } from 'react-router-dom';
import Home from './Home';
import Users from './Users';
import Contact from './Contact';
import NotFound from './NotFound';

function App() {
  const routing = (
    <Router>
      <div className="container">
        <ul>
          <li> <Link to="/">Home</Link> </li>
          <li> <Link to="/users">Users</Link> </li>
          <li> <Link to="/contact">Contact</Link> </li>
        </ul>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/users/:id" component={Users} />
          <Route path="/contact" component={Contact} />
          <Route component={NotFound} />
      </Switch>
      </div>
    </Router>
  )

  return (
    <div className="App">
      {routing}
    </div>
  );
}

export default App;
