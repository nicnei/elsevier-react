import React, {useState,useEffect} from 'react';
import './App.css';
import axios from 'axios';

function App() {
  const [users, setUsers] = useState([])

  useEffect(() => {
    const getData = async() => {
      let response = await axios.get('http://localhost:4000/users')
      let users = await response.data
      setUsers(users)
    }
    getData()
  },[])

  return(
    <ul>
      {users.map(user => <li key={user.id}>{user.email}</li>)}
    </ul>
  )
}

export default App;
