import React from 'react';

class Message extends React.Component {
    constructor() {
        super()
        this.state = {message: 'Hello stranger'}
    }
    changeMessage() {
        this.setState({message: 'Hello Nic'})
    }
    render() {
        return (
            <div>
                <h1>{this.state.message}</h1>
                <button onClick={ () => this.changeMessage() } > Click me!</button>
            </div>
        )
    }
}

export default Message