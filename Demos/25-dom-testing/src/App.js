import React from 'react';
import './App.css';
import CheckboxWithLabel from './CheckboxWithLabel';

function App() {
  return (
    <div className="App">
      <CheckboxWithLabel labelOn='Hello' labelOff='Goodbye'/>
    </div>
  );
}

export default App;
