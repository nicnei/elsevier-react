import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Enzyme, {shallow} from 'enzyme';
import CheckboxWithLabel from "./CheckboxWithLabel";
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

it ("CheckboxWithLabel changes the text after click", () => {
  const checkbox = shallow(<CheckboxWithLabel labelOn="Hello" labelOff="Goodbye"/>);
  expect(checkbox.text()).toEqual("Goodbye");
  checkbox.find("input").simulate("change");
  expect(checkbox.text()).toEqual("Hello");
});

