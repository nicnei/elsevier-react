import React, { useState } from "react"

const CheckboxWithLabel = props => {
    const [isChecked, setIsChecked] = useState(false);
    const onChange = () => {
        setIsChecked(!isChecked)
    }
    return (
        <label htmlFor="Checkbox">
            <input type="checkbox" name="check" checked={isChecked} onChange={onChange} />
            {isChecked ? props.labelOn : props.labelOff}
        </label>
    )
}

export default CheckboxWithLabel;
