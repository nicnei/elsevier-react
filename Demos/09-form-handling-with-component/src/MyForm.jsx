import React, { Component } from 'react'

class MyForm extends Component {
   state = {email: '', city: 'philadelphia'}
   handleEmailChange = (event) => {
        this.setState({email: event.target.value})
   }
   handleCityChange = (event) => {
       this.setState({city: event.target.value})
   }
   handleSubmit = (event) => {
       alert(`${this.state.email} ${this.state.city}`)
       event.preventDefault() //stops browser clearing form
   }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div> 
                <label>Email: </label>
                <input type='email' value={this.state.email} onChange={this.handleEmailChange} required></input>
                </div>
                <div>
                <label>City: </label>
                <select value={this.state.city} onChange = {this.handleCityChange}>
                        <option value='philadelphia'>Philadelphia</option>
                        <option value='new york'>New York</option>
                        <option value='london'>London</option>
                </select>
                </div>
                <button type='submit'>Submit</button>
            </form>
        )
    }
}

export default MyForm