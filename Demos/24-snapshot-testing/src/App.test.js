import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import renderer from 'react-test-renderer';
import Login from './Login';

it('renders correctly', () => {
  const tree = renderer.create(<Login></Login>).toJSON();
  expect(tree).toMatchSnapshot();
});

