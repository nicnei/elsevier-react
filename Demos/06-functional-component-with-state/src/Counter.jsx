import React, {useState, useEffect} from 'react'

const Counter = () => {
    const [count, setCount] = useState(0)
    const addCount = () => setCount(count + 1)

    useEffect( () => {
        document.title = `You clicked ${count} times`
    })
    return (
        <div>
            <h3>You clicked {count} times</h3>
            <button onClick={addCount}> Click Me! </button>
        </div>
    )
}

export default Counter