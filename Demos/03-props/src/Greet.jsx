import React from 'react';
import PropTypes from 'prop-types';

const Greet = props => {
    return (
        <div>
            Hello {props.name}<br/>
            Your monthly salary is ${props.annualPay/12}
        </div>)
}

Greet.propTypes = {
    name: PropTypes.string.isRequired,
    annualPay: PropTypes.number.isRequired
}

Greet.defaultProps = {
    name: "John Doe",
    annualPay: 0
}

export default Greet;