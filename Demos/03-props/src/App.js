import React from 'react';
import './App.css';
import Greet from './Greet';

function App() {
  return (
    <div>
      <Greet name="Fred" annualPay={60000}/>
    </div>
  );
}



export default App;
