import React, {useState,useEffect} from 'react';
import './App.css';

function App() {
  const [users, setUsers] = useState([])

  useEffect(() => {
    let getData = async() => {
      let response = await fetch('http://localhost:4000/users')
      let users = await response.json()
      setUsers(users)
    }
    getData()
  },[])

  return(
    <ul>
      {users.map(user => <li key={user.id}>{user.email}</li>)}
    </ul>
  )
}

export default App;
