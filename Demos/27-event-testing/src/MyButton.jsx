import React from "react";

const MyButton = props => {
    const clicked =  props.clickMe;
    return (<button onClick={clicked}>Click Me</button>)
}

export default MyButton;
