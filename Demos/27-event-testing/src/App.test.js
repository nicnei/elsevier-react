import React from "react";
import Enzyme, { shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import MyButton from "./MyButton";

Enzyme.configure({ adapter: new Adapter() });

it ("Tests that MyButton fires onClick function from parent when clicked", () => {
  // Mock the function from props
  const props = { clickMe: jest.fn() };
  // Render an instance component with props
  const testComponent = shallow( <MyButton {...props} />);
  // Find the button an simulate a click
  testComponent.find("button").simulate("click");
  // Expect mock function has been called once
  expect(props.clickMe.mock.calls.length).toBe(1);
});
