import React from 'react';
import './App.css';
import MyButton from './MyButton';

const clickMe = () => {
  console.log('Clicked me')
}

function App() {
  return (
    <div className="App">
      <MyButton>clickMe={clickMe}</MyButton>
    </div>
  );
}

export default App;
