import React, {Component} from 'react';

class NotFound extends Component {
    render() {
        return <h3>This page is not found</h3>
    }
}

export default NotFound