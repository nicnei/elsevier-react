import React, {useState} from 'react';
import './App.css';
import MyForm from './MyForm';

function App() {
  const [email,setEmail] = useState('not given')
  const [city,setCity] = useState('not given')

  const showEmail = email => {
    setEmail(email)
  }
  const showCity = city => {
    setCity(city)
  }
 
  return (
    <div className="App">
      <h3>Email: {email}</h3>
      <h3>City: {city}</h3>
      <MyForm submitEmail={showEmail} submitCity={showCity}/>
    </div>
  );
}

export default App;
