import React, {useState,useEffect} from 'react';
import './App.css';

function App() {
  const [users, setUsers] = useState([])

  useEffect(() => {
    fetch('http://localhost:4000/users')
      .then(response => response.json())
      .then(data => { setUsers(data) })
  },[])

  return(
    <ul>
      {users.map(user => <li key={user.id}>{user.email}</li>)}
    </ul>
  )
}

export default App;
