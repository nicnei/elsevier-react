import React from 'react';
import './App.css';
import Footer from './Footer';

function App() {
  return (
    <div className="App">
      <h3>Hello world</h3>
      <Footer/>
    </div>
  );
}

export default App;
