import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Footer from './Footer';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

it('renders the footer', () => {
  const wrapper = shallow(<App />);
  expect(wrapper.find('Footer').exists()).toBeTruthy()
});
