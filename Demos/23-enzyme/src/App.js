import React, {useState} from 'react'
import './App.css';

function App() {
  const [count, setCount] = useState(0)
  const addCount = () => setCount(count + 1)

  return (
    <div className="App">
      <h3>You clicked {count} times</h3>
      <button onClick={addCount}> Click Me! </button>
    </div>
  );
}

export default App;
