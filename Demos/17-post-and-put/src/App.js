import React, {useState,useEffect} from 'react';
import './App.css';
import axios from 'axios';

function App() {
  const [users, setUsers] = useState([])

  useEffect(() => {
    const getData = async() => {
      let response = await axios.get('http://localhost:4000/users')
      let users = await response.data
      setUsers(users)
    }
    getData()
  },[])

  const postPebbles = () => {
    const pebbles = {
      "id": 4,
      "first_name": "Pebbles",
      "last_name": "Flintstone",
      "email": "pebbles@gmail.com"
    }
    axios.post('http://localhost:4000/users', pebbles); 
  }

  const updateFred = () => {
    const fred = {
      "id": 1,
      "first_name": "Fred",
      "last_name": "Flintstone",
      "email": "fredflintstone@gmail.com"
    }
    axios.put('http://localhost:4000/users/1', fred); 
    console.log('updateFred')
  }
  const postFail = () => {
    const fred = {
      "id": 1,
      "first_name": "Fred",
      "last_name": "Flintstone",
      "email": "fred@gmail.com"
    }
    axios.post('http://localhost:4000/users/1', fred).catch(error => {
      alert("We have a problem")
    }); 
    console.log('post fail')
  }
  return(
    <ul>
      {users.map(user => <li key={user.id}>{user.email}</li>)}
      <button onClick={postPebbles}>Add Pebbles</button>
      <button onClick={updateFred}>Update Fred</button>
      <button onClick={postFail}>Post Fail Example</button>
    </ul>
  )
}

export default App;
