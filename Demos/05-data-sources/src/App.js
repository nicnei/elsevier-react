import React from 'react';
import './App.css';
import CountryList from './CountryList';

function App() {
  return (
    <CountryList/>
  );
}

export default App;
