import React from 'react';
import Country from './Country';
import './CountryList.css';

const CountryList = () => {
    const countries = [
        {name: 'USA',    city: 'Washington DC', population: 327000000},
        {name: 'UK',     city: 'London',        population: 66000000}, 
        {name: 'France', city: 'Paris',         population: 67000000}
    ]
    countries.push({name: 'Spain',city: 'Madrid'})
    const countryList = countries.map(country => <Country key={country.name} country={country} />)
 
    return (
        <div className="center-div">
            <h3>Countries and their Cities</h3>
            <table border="1" id="countries">
                <thead>
                    <tr>
                        <th>Country</th>
                        <th>City</th>
                    </tr>
                </thead>
                <tbody>
                    {countryList}
                </tbody>
            </table>
        </div>
    );
}

export default CountryList