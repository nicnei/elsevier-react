import React from 'react';

const Country = ({country}) => {
   return (
        <tr>
            <td>{country.name}</td>
            <td>{country.city}</td>
        </tr>
    )
}

export default Country