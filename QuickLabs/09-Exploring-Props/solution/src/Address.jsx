import React from 'react';
import PropTypes from 'prop-types';

const Address = props => {
    console.log(props)
    return (
        <address>
            {props.street}<br/>
            {props.zip}<br/>
            {props.state}<br/>
            Floor: {props.floor}<br/>
        </address>
    )
}

Address.defaultProps = {
    street: `Unknown street`,
    zip:    10000, 
    city:   `Unknown city`,
    floor:  `Unknown floor`
 } 
  
export default Address;