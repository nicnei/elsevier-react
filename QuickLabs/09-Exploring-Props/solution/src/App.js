import React from 'react';
import './App.css';

import Address from './Address';

function App() {
  return (
    <div>
      <p>Location:</p>
      <Address street="1600 John F Kennedy Boulevard"
        zip={19103}
        city="Philadelphia"
        floor="16th Floor"
      />
    </div>
  );
}

export default App;