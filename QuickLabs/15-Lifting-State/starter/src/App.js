import React, {useState} from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import Header from './Components/Header';
import Footer from './Components/Footer';
import AllTodos from './Components/AllTodos';
import AddTodo from './Components/AddTodo';
import sampleTodos from './sampleTodos.json'

function App() {
  const [todos, setTodos] = useState(sampleTodos)
  return (
    <div className="container">
      <Header />
        <div className="container">
          <AllTodos todos={todos}/>
          <AddTodo/>
        </div>
      <Footer/>
    </div>
  )
}

export default App
