import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import Header from './Components/Header';
import Footer from './Components/Footer';
import AllTodos from './Components/AllTodos';
import AddTodo from './Components/AddTodo';

function App() {
  return (
    <div className="container">
      <Header />
        <div className="container">
          <AllTodos/>
          <AddTodo/>
        </div>
      <Footer/>
    </div>
  )
}

export default App
